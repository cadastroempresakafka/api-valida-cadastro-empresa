package br.com.novapat.empresa.client;

import br.com.novapat.empresa.models.Empresa;

import java.util.Optional;

public class ReceitaClientFallback implements ReceitaClient {
    
    @Override
    public Optional<Empresa> findByCNPJ(String cnpj) {
        throw new ReceitaOfflineException();
    }
}
