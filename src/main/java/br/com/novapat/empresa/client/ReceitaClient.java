package br.com.novapat.empresa.client;

import br.com.novapat.empresa.models.Empresa;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Optional;


@FeignClient(url = "http://www.receitaws.com.br/v1/cnpj", name="receita", configuration = ReceitaClientConfiguration.class)
public interface ReceitaClient {

        @GetMapping("/{cnpj}")
        Optional<Empresa> findByCNPJ(@PathVariable(value="cnpj") String cnpj);

}
