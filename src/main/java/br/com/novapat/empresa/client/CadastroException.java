package br.com.novapat.empresa.client;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Os dados informados são inválido")
public class CadastroException extends RuntimeException {
}
