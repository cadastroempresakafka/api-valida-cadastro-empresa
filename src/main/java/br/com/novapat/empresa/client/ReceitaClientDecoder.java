package br.com.novapat.empresa.client;

import feign.Response;
import feign.codec.ErrorDecoder;

public class ReceitaClientDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if (response.status() == 404) {
            return new CadastroException();
        }else{
            return errorDecoder.decode(s, response);
        }
    }

}
