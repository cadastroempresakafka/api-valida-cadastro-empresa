package br.com.novapat.empresa.client;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code= HttpStatus.BAD_GATEWAY, reason = "O sistema da receita está offline!")
public class ReceitaOfflineException extends RuntimeException {
}
