package br.com.novapat.empresa.client;

import feign.Feign;
import feign.RetryableException;
import feign.codec.ErrorDecoder;
import io.github.resilience4j.feign.FeignDecorator;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.springframework.context.annotation.Bean;

public class ReceitaClientConfiguration {

    @Bean
    public ErrorDecoder getErrorDecoder() {
        return new ReceitaClientDecoder();
    }

    @Bean
    public Feign.Builder builder(){
        FeignDecorator feignDecorators = FeignDecorators.builder()
                .withFallback(new ReceitaClientFallback(), RetryableException.class)
                .build();
        return Resilience4jFeign.builder(feignDecorators);
    }


}
