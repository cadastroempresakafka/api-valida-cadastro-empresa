package br.com.novapat.empresa.client;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Erro ao consultar o CNPJ")
public class ConsultaCNPJException extends RuntimeException {

    public ConsultaCNPJException(String cnpj) {
    }
}
