package br.com.novapat.empresa.cadastro;

import br.com.novapat.empresa.models.Empresa;
import br.com.novapat.empresa.service.ValidadorCapitalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Component
public class CadastroEmpresaListener {

    @Autowired
    private ValidadorCapitalService validadorCapitalService;

    @KafkaListener(topics = "spec3-patricia-novaes-2", groupId = "PATI-NOVAES-1")
    public void receber(@Payload Empresa empresa){

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");

        System.out.println("Atenção, a empresa: "+ empresa.getNome()+
                           " com o cnpj: " + empresa.getCnpj() +
                           " foi cadastrada. Data de cadastro: " + LocalDateTime.now().format(formatter));

        validadorCapitalService.validarCapital(empresa.getCnpj());
    }

}
