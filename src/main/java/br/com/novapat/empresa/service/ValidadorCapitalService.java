package br.com.novapat.empresa.service;

import br.com.novapat.empresa.client.ConsultaCNPJException;
import br.com.novapat.empresa.client.ReceitaClient;
import br.com.novapat.empresa.models.Empresa;
import br.com.novapat.empresa.models.ResultadoValidacaoEmpresa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ValidadorCapitalService {

    @Autowired
    private ResultadoValidacaoService resultadoValidacaoService;

    @Autowired
    private ReceitaClient receitaClient;

    public void validarCapital(String cnpj) {
        Empresa empresaConsultada = receitaClient
                .findByCNPJ(cnpj)
                .filter(e -> e.getCapitalSocial() != null)
                .filter(e -> e.getCnpj() != null)
                .orElseThrow(() -> new ConsultaCNPJException(cnpj));

        ResultadoValidacaoEmpresa resultado = criaResultado(empresaConsultada);
        resultadoValidacaoService.salvaResultadoValidacao(resultado);
    }

    private ResultadoValidacaoEmpresa criaResultado(Empresa empresa) {
        ResultadoValidacaoEmpresa resultado = new ResultadoValidacaoEmpresa();
        resultado.setCnpj(empresa.getCnpj());
        resultado.setNome(empresa.getNome());
        resultado.setCapitalSocial(empresa.getCapitalSocial());
        resultado.setCapitalAceito(isCapitalAcimaDeUmMilhao(empresa.getCapitalSocial()));
        return resultado;
    }

    private boolean isCapitalAcimaDeUmMilhao(String capitalSocial) {
        return Double.parseDouble(capitalSocial) > 1000000.00;
    }
}
