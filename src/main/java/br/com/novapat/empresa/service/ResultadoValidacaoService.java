package br.com.novapat.empresa.service;


import br.com.novapat.empresa.models.ResultadoValidacaoEmpresa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class ResultadoValidacaoService {


    @Autowired
    private KafkaTemplate<String, ResultadoValidacaoEmpresa> sender;

    public void salvaResultadoValidacao(ResultadoValidacaoEmpresa resultado) {
        sender.send("spec3-patricia-novaes-3", "1", resultado);
    }
}
